package pm

import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(UserService)
@Mock(User)
class UserServiceSpec extends Specification {

	def setup() {
		def user = new User(username: 'test', password: 'test').save()
	}

	void 'test login with correct credentials'() {
		expect:
		service.checkLoginCredentials('test', 'test')
	}

	void 'test login with incorrect username'() {
		expect:
		!service.checkLoginCredentials('test1', 'test')
	}

	void 'test login with incorrect password'() {
		expect:
		!service.checkLoginCredentials('test', 'test1')
	}

	void 'test login with null username'() {
		expect:
		!service.checkLoginCredentials(null, 'test')
	}

	void 'test login with null password'() {
		expect:
		!service.checkLoginCredentials('test', null)
	}

	void 'test login with empty username and password'() {
		expect:
		!service.checkLoginCredentials('', '')
	}
}
