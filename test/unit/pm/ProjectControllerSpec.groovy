package pm

import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(ProjectController)
class ProjectControllerSpec extends Specification {
	def projectService = Mock(ProjectService)

	def setup() {
		controller.projectService = projectService
	}

	void 'test start page'() {
		when:
		controller.index()
		then:
		response.redirectedUrl == '/project/list'
	}
	
	void 'test show with valid ID'() {
		given:
		controller.params.id = 1
		projectService.get(1) >> new Project()
		when:
		def model = controller.show()
		then:
		model.project
	}
	
	void 'test create'() {
		when:
		def model = controller.create()
		then:
		model.project
	}
	
	void 'test save redirect'() {
		when:
		request.method = 'GET'
		controller.save()
		then:
		response.redirectedUrl == '/project/create'
	}

}
