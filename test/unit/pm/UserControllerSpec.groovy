package pm

import grails.test.mixin.TestFor
import grails.test.mixin.Mock
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.web.ControllerUnitTestMixin} for usage instructions
 */
@TestFor(UserController)
class UserControllerSpec extends Specification {
	def userService = Mock(UserService)

	def setup() {
		controller.userService = userService
	}

	void 'test start page'() {
		when:
		controller.index()
		then:
		response.redirectedUrl == '/user/login'
	}

	void 'test login with correct credentials'() {
		given:
		controller.params.username = 'test'
		controller.params.password = 'test'
		userService.checkLoginCredentials('test', 'test') >> new User(username: 'test', password:  'test')

		when:
		request.method = 'POST'
		controller.login()
		then:
		!flash.message
		response.redirectedUrl == '/project/list'
		session.user
	}

	void 'test login with incorrect credentials'() {
		given:
		controller.params.username = 'test'
		controller.params.password = 'test'
		userService.checkLoginCredentials('test', 'test') >> null

		when:
		request.method = 'POST'
		controller.login()
		then:
		flash.message == 'Incorrect username or password'
		!session.user
	}
	
	void 'test logout'() {
		when:
		controller.logout()
		then:
		!session.user
	}
}
