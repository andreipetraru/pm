package pm

import grails.test.mixin.Mock
import grails.test.mixin.TestFor
import spock.lang.Specification

/**
 * See the API for {@link grails.test.mixin.services.ServiceUnitTestMixin} for usage instructions
 */
@TestFor(ProjectService)
@Mock([User, Project])
class ProjectServiceSpec extends Specification {

    def setup() {
		def u1 = new User(username: 'admin', password: 'admin').save(failOnError: true)
		def u2 = new User(username: 'user', password: 'user').save(failOnError: true)

		def p1 = new Project(name: "Project X", code: "px", techLead: "John",
				projectManager: "Adam", deliveryDate: new Date(), phase: Phase.DEVELOPMENT,
				priority: 5, user: u1)
		def p2 = new Project(name: "Project Y", code: "py", techLead: "Richard",
				projectManager: "Eve", deliveryDate: new Date(), phase: Phase.BRIEFING,
				priority: 3, user: u1)
		def p3 = new Project(name: "Project Z", code: "pz", techLead: "Alex",
				projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
				priority: 4, user: u2)
		service.save(p1)
		service.save(p2)
		service.save(p3)
    }

    void 'three projects in the database'() {
		expect:
		service.getAll().size == 3
    }
	
	void 'test save with valid project'() {
		when:
		def proj = new Project(name: "Project ZZ", code: "pzz", techLead: "Alex",
			projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
			priority: 1, user: User.get(1))
		then:
		service.save(proj)		
		service.getAll().size == 4
	}
	
	void 'test save with invalid project'() {
		expect:
		!service.save(new Project())
	}
	
	void 'test get all returns data sorted by priority'() {
		expect:
		service.getAll().size == 3
		service.getAll()[0].priority == 3
	}
	
	void 'test get with valid ID'() {
		when:
		def id = service.getAll()[0].id
		then:
		service.get(id)
	}
	
	void 'test get with invalid ID'() {
		when:
		def id = service.getAll().size + 1
		then:
		!service.get(id)
	}	
	
}
