<html>
<head>
<title>Login</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
#login {
	background-color: darkred;
	color: white;
	text-align: left;
	padding: 5px 15px;
	height: 45px;
}

#login a {
	color: white;
}
</style>
</head>
<body>
	<div id="login">
		<g:if test="${session.user}">
			Welcome back ${session.user.name}
			<g:link action="logout">Logout</g:link>
		</g:if>
		<g:else>
			<g:form controller="user" action="login">
				<input type="text" name="username" placeholder="Username" />
				<input type="password" name="password" placeholder="Password" />
				<input type="submit" value="Login" />
				<br />
				<g:if test="${flash.message}">
					${flash.message}
				</g:if>
			</g:form>
		</g:else>
	</div>
</body>
</html>