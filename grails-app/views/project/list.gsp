<html>
<head>
<title>Projects</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
.right {
	float: right;
	clear: both;
}
</style>
</head>
<body>
	<div class="right">
		${session.user}
		<br />
		<g:link controller="user" action="logout">Logout</g:link>
	</div>
	<ul>
		<g:link controller="project" action="create">New Project</g:link>
		<g:link controller="project" action="list">View</g:link>
	</ul>
	<table>
		<caption>All Projects</caption>
		<th>Priority</th>
		<th>Reprioritize</th>
		<th>Name</th>
		<th>Code</th>
		<th>Tech Lead</th>
		<th>Project Manager</th>
		<th>Delivery Date</th>
		<th>Phase</th>
		<th>Created By</th>
		<g:each in="${projects}" var="project">
			<tr>
				<td><g:link controller="project" action="show"
						id="${project.id}">
						${project.priority}
					</g:link></td>
				<td><g:link controller="project" action="reprioritize"
						id="${project.id}">
						Reprioritize
					</g:link></td>
				<td>
					${project.name}
				</td>
				<td>
					${project.code}
				</td>
				<td>
					${project.techLead}
				</td>
				<td>
					${project.projectManager}
				</td>
				<td><g:formatDate format="dd/MMM/yyyy"
						date="${project.deliveryDate}" /></td>
				<td>
					${project.phase}
				</td>
				<td>
					${project.user}
				</td>
			</tr>
		</g:each>
	</table>
</body>
</html>