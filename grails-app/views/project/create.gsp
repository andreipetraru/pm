<%! import pm.Phase %>

<html>
<head>
<title>Welcome to Grails</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
.right {
	float: right;
	clear: both;
}

ul {
	list-style-type: none;
}
</style>
</head>
<body>
	<div class="right">
		${session.user}
		<br />
		<g:link controller="user" action="logout">Logout</g:link>
	</div>
	<ul>
		<g:link controller="project" action="create">New Project</g:link>
		<g:link controller="project" action="list">View</g:link>
	</ul>
	<div id="create">
		<g:form controller="project" action="save">
			Name
			<br />
			<input type="text" name="name" placeholder="Name" />
			<br />
			Code
			<br />
			<input type="text" name="code" placeholder="Code" />
			<br />
			Tech Lead
			<br />
			<input type="text" name="techLead" placeholder="Tech Lead" />
			<br />
			Project Manager
			<br />
			<input type="text" name="projectManager"
				placeholder="Project Manager" />
			<br />
			Delivery Date
			<br />
			<g:datePicker name="deliveryDate" precision="day" />
			<br />
			Phase
			<br />
			<g:select name="phase" from="${Phase.values()}" />
			<br />
			Priority
			<br />
			<input type="text" name="priority" placeholder="Priority" />
			<br />
			<input type="submit" value="Create" />
			<br />
			<div style="color: red; margin-left: 10px;">
				${flash.message}
				<br />
				<g:renderErrors bean="${project}" />
			</div>
		</g:form>
	</div>
</body>
</html>