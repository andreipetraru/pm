<html>
<head>
<title>Projects</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
.right {
	float: right;
	clear: both;
}
</style>
</head>
<body>
	<ul>
		<g:link controller="user" action="login">Login</g:link>
		<g:link controller="project" action="create">New Project</g:link>
		<g:link controller="project" action="list">View</g:link>
	</ul>
	<ul class="errors">
		<li>An error has occurred</li>
	</ul>
</body>
</html>
