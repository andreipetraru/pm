<%! import pm.Phase %>

<html>
<head>
<title>${project.name}</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
.right {
	float: right;
	clear: both;
}

ul {
	list-style-type: none;
}
</style>
</head>
<body>
	<div class="right">
		${session.user}
		<br />
		<g:link controller="user" action="logout">Logout</g:link>
	</div>
	<ul>
		<g:link controller="project" action="create">New Project</g:link>
		<g:link controller="project" action="list">View</g:link>
	</ul>
	<div id="priority">
		<g:form controller="project">
			<g:hiddenField name="id" value="${project.id}"/>
			${project.name}
			<br>
			Current priority: ${project.priority}
			<br />
			Change priority
			<br/>
			<g:select name="priority" from="${priorities}" noSelection="${['null':'Select One...']}"
				value="${newPriority}" />
			<br />
			<g:actionSubmit value="Update" action="updatePriority" />
			<br />
			<div style="color: red; margin-left: 10px;">
				<g:renderErrors bean="${project}" />
			</div>
		</g:form>
	</div>
</body>
</html>