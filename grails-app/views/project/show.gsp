<%! import pm.Phase %>

<html>
<head>
<title>
	${project}
</title>
<meta name="layout" content="main" />
<style type="text/css" media="screen">
.right {
	float: right;
	clear: both;
}

ul {
	list-style-type: none;
}
</style>
</head>
<body>
	<div class="right">
		${session.user}
		<br />
		<g:link controller="user" action="logout">Logout</g:link>
	</div>
	<ul>
		<g:link controller="project" action="create">New Project</g:link>
		<g:link controller="project" action="list">View</g:link>
	</ul>
	<g:form controller="project">
		<g:hiddenField name="id" readonly value="${project.id}" />
			Name
			<br />
		<input type="text" name="name" placeholder="Name"
			value="${project.name}" />
		<br />
			Code
			<br />
		<input type="text" name="code" placeholder="Code"
			value="${project.code}" />
		<br />
			Tech Lead
			<br />
		<input type="text" name="techLead" placeholder="Tech Lead"
			value="${project.techLead}" />
		<br />
			Project Manager
			<br />
		<input type="text" name="projectManager" placeholder="Project Manager"
			value="${project.projectManager}" />
		<br />
			Delivery Date
			<br />
		<g:datePicker name="deliveryDate" precision="day"
			value="${project.deliveryDate}" />
		<br />
			Phase
			<br />
		<g:select name="phase" from="${Phase.values()}"
			value="${project.phase}" />
		<br />
			Priority
			<br />
		<input type="text" name="priority" placeholder="Priority" disabled
			value="${project.priority}" />
		<br />
		<g:actionSubmit value="Update" action="update" />
		<g:actionSubmit value="Delete" action="delete" />
		<div style="color: red; margin-left: 10px;">
			<g:renderErrors bean="${project}" />
		</div>
	</g:form>
</body>
</html>