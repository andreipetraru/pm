package pm

class UserController {
	def userService

	def index = {  redirect(action: 'login')  }

	def login = {
		if (request.method == 'POST') {
			def user = userService.checkLoginCredentials(params.username, params.password)
			if (user) {
				session.user = user
				redirect(controller: 'project', action: 'list')
			}
			else{
				flash.message = 'Incorrect username or password'
			}
		}
	}

	def logout = {
		session.invalidate()
		redirect(action: 'login')
	}
}
