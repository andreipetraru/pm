package pm

import pm.Phase

class ProjectController {

	def projectService

	def beforeInterceptor = [action: this.&auth, except: 'login']

	private auth() {
		if (!session.user) {
			redirect(controller: 'user', action: 'login')
			return false
		}
	}

	def list() {
		def projects = projectService.getAll()
		print projects
		[projects: projects]
	}

	def index() {
		redirect(action: 'list')
	}

	def show() {
		Project project = projectService.get(params.id)
		if (project) {
			[project: project]
		}
		else {
			redirect('error')
		}
	}

	def create() {
		def project = new Project()
		[project: project]
	}

	def save(Project project) {
		if (request.method == 'POST') {
			def duplicatePriority = projectService.getByPriority(project.priority)
			if (duplicatePriority) {
				flash.message = 'Priority already exists'
				render(view: 'create', model:[project: project])
				return
			}
			project.user = session.user
			if (!projectService.save(project)) {
				render(view: 'create', model:[project: project])
				return
			}
			else {
				redirect(action: 'list')
			}
		}
		else {
			redirect(action: 'create')
		}
	}

	def update(Project project) {
		def proj = projectService.get(params.id)
		proj.properties = params
		if (!projectService.save(proj)) {
			render(view: 'show', model:[project: proj])
			return
		}
		else {
			redirect(action: 'list')
		}
	}

	def reprioritize() {
		Project proj = projectService.get(params.id)
		if (proj) {
			[project: proj, priorities: projectService.getPriorities(proj.priority)]
		}
		else {
			redirect('error')
		}
	}

	def updatePriority() {
		Integer newPriority = params?.int('priority')
		if (newPriority) {
			Project proj = projectService.get(params.id)
			Integer oldPriority = proj.priority

			List<Project> projectsToUpdate = projectService.getByPriorities(oldPriority, newPriority)
			def priorities = []
			projectsToUpdate.each { priorities << it.priority }

			Collections.rotate(priorities, oldPriority > newPriority ? -1 : 1)

			for (int i = 0; i < priorities.size(); i++) {
				projectsToUpdate[i].priority = priorities[i]
				projectService.save(projectsToUpdate[i])
			}
			redirect(action: 'list')
		}
		else {
			redirect(action: 'reprioritize')
			return
		}
	}

	def delete(Project project) {
		def proj = projectService.get(params.id)
		projectService.delete(proj)
		redirect(action: 'list')
	}
}