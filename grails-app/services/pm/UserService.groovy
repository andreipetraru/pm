package pm

import org.springframework.transaction.annotation.Transactional

class UserService {
	@Transactional(readOnly = true)
	def checkLoginCredentials(String username, String password) {
		User.findByUsernameAndPassword(username, password)
	}
}
