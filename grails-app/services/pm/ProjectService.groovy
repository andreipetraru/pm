package pm

import grails.transaction.Transactional

@Transactional
class ProjectService {
	def save(Project project) {
		project.save(flush: true)
	}

	def delete(Project project) {
		project.delete(flush: true)
	}

	@Transactional(readOnly = true)
	def getAll() {
		Project.getAll().sort()
	}

	@Transactional(readOnly = true)
	def get(id) {
		Project.get(id)
	}
	
	@Transactional(readOnly = true)
	def getPriorities(Integer priority) {
		Project.executeQuery('select priority from Project where priority != ? order by priority', [priority])
	}

	@Transactional(readOnly = true)
	def getByPriorities(oldPriority, newPriority) {
		Project.executeQuery('from Project where priority between ? and ? order by priority', 
			[Math.min(oldPriority, newPriority), Math.max(oldPriority, newPriority)])
	}
	
	def getByPriority(Integer priority) {
		Project.findByPriority(priority)
	}
}
