import pm.Project
import pm.User
import pm.Phase

class BootStrap {

	def init = { servletContext ->
        def u1 = new User(username: 'admin', password: 'admin').save(failOnError: true)
        def u2 = new User(username: 'user', password: 'user').save(failOnError: true)

        def p1 = new Project(name: "5", code: "px", techLead: "John",
                projectManager: "Adam", deliveryDate: new Date(), phase: Phase.DEVELOPMENT,
                priority: 5, user: u1).save(failOnError: true)

        def p2 = new Project(name: "3", code: "py", techLead: "Richard",
                projectManager: "Eve", deliveryDate: new Date(), phase: Phase.BRIEFING,
                priority: 3, user: u1).save(failOnError: true)

        def p3 = new Project(name: "4", code: "pz", techLead: "Alex",
                projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
                priority: 4, user: u2).save(failOnError: true)
				
        def p4 = new Project(name: "2", code: "pa", techLead: "Alex",
                projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
                priority: 2, user: u2).save(failOnError: true)

        def p5 = new Project(name: "1", code: "pb", techLead: "Alex",
                projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
                priority: 1, user: u2).save(failOnError: true)

        def p6 = new Project(name: "6", code: "pc", techLead: "Alex",
                projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
                priority: 6, user: u2).save(failOnError: true)

        def p7 = new Project(name: "7", code: "pd", techLead: "Alex",
                projectManager: "Noel", deliveryDate: new Date(), phase: Phase.QA,
                priority: 7, user: u2).save(failOnError: true)
	}
	def destroy = {
	}
}
