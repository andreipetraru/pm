package pm

class Project implements Comparable<Project> {
	String name
	String code
	String techLead
	String projectManager
	Date deliveryDate
	Phase phase
	Integer priority

	static belongsTo = [user: User]

	static constraints = {
		name(blank:false, unique: true)
		code(blank:false, unique: true)
		techLead(blank:false)
		projectManager(blank:false)
	}

	@Override
	String toString() {
		name + "(" + priority + ")"
	}

	@Override
	int compareTo(Project o) {
		return priority - o.priority
	}
}
