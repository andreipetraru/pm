package pm

class User {
	String username
	String password

	static constraints = {
		username(blank: false)
		password(blank:false, password: true)
	}

	static hasMany = [projects: Project]

	static mapping = { projects lazy: false }

	@Override
	String toString() {
		username
	}
}
